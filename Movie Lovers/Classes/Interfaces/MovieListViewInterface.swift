//
//  MovieListViewInterface.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 30/11/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import Foundation

protocol MovieListViewInterface {
    func animateLogo()
    func animateSplash()
    func loadMovieList(_ movieList: [MovieContent])
    func showErrorView()
    func hideSplash()
    func setupNavigationBar()
    func hideNavgationBar()
    func presentMovieDetailScreenWith(_ movie: MovieContent)
}

protocol MovieListModuleInterface {
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
    func selectMovieAtIndex(_ index: Int)
}

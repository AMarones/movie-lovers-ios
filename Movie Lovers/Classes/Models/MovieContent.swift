//
//  MovieContent.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 4/24/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import UIKit
import SWXMLHash

struct MovieContent: XMLIndexerDeserializable {
    var name: String?
    var genre: String?
    var plot: String?
    var posterImageUrl: String?
    var mainImageUrl: String?
    var rating: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> MovieContent {
        return MovieContent(
            name            : node.element?.attributes["Descricao"],
            genre           : node.element?.attributes["Genero"],
            plot            : node.element?.attributes["ConteudoSinopse"],
            posterImageUrl  : node.element?.attributes["FiguraDestaque"],
            mainImageUrl    : node.element?.attributes["FiguraHorizontal"],
            rating          : node.element?.attributes["Classificacao"]
        )
    }}

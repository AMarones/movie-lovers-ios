//
//  MovieListInteractorIO.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 30/11/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import Foundation

protocol MovieListInteractorInput {
    func findMovieList()
    func findMovieAtIndex(_ index: Int)
}

protocol MovieListInteractorOutput {
    func movieListDidLoad()
    func foundMovieList(_ movieList: [MovieContent])
    func movieListNotFound()
    func movieListWithInternalError()
    func foundMoviePlot(movie: MovieContent)
}

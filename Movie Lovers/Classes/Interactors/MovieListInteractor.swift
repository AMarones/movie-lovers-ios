//
//  MovieListInteractor.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 30/11/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import Foundation

class MovieListInteractor: NSObject {
    
    var output                  : MovieListInteractorOutput?
    var movieList               = [MovieContent]()
    
    init(output: MovieListInteractorOutput) {
        super.init()
        self.output = output
    }
}


extension MovieListInteractor: MovieListInteractorInput {
    
    func findMovieList() {
        let movieService = MovieService()
        
        movieService.loadMovieList { (movies) in
            self.movieList = movies
            
            self.output?.movieListDidLoad()
            
            if self.movieList.count > 0 {
                self.output?.foundMovieList(movies)
            } else {
                self.output?.movieListNotFound()
            }
        }

    }
    
    func findMovieAtIndex(_ index: Int) {
        let moviePlot = movieList[index]
        output?.foundMoviePlot(movie: moviePlot)
    }
}

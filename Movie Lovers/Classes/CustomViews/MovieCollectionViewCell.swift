//
//  MovieCollectionViewCell.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 4/24/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import UIKit
import Kingfisher

class MovieCollectionViewCell: UICollectionViewCell {
 
    var movie: MovieContent?
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imagePoster: UIImageView!
    
    func setupMovieContent(_ movie: MovieContent) {
        if let URLString = movie.posterImageUrl {
            let URL = Foundation.URL(string:URLString)!
            
            imagePoster.kf.setImage(with: URL,
                             placeholder: nil,
                             options: [
                                .transition(.fade(1)),
                                .processor(DefaultImageProcessor.default)
                ],
                             progressBlock: nil,
                             completionHandler: nil)
        }
        
        labelTitle.text = movie.name
    }

}

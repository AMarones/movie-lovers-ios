//
//  MovieDetailViewController.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 4/24/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import UIKit
import Kingfisher

class MovieDetailViewController: UIViewController {

    var movieContent: MovieContent!
    
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var labelMovieName: UILabel!
    @IBOutlet weak var labelMoviePlot: UILabel!
    @IBOutlet weak var labelMovieGenreStatic: UILabel!
    @IBOutlet weak var labelMovieGenre: UILabel!
    @IBOutlet weak var labelMovieRatingStatic: UILabel!
    @IBOutlet weak var labelMovieRating: UILabel!
    
    @IBOutlet var genreCollection: NSArray!
    @IBOutlet var RatingCollection: NSArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContentView()
    }
    
    func loadContentView() {
        labelMovieName.text = movieContent.name
        labelMoviePlot.text = movieContent.plot
        
        if let URLString = movieContent.mainImageUrl {
            let URL = Foundation.URL(string:URLString)!
            imageHeader.kf.setImage(with: URL,
                                    placeholder: nil,
                                    options: [
                                        .transition(.fade(1)),
                                        .processor(DefaultImageProcessor.default)
                ],
                                    progressBlock: nil,
                                    completionHandler: nil)
        }
        
        if let genre = movieContent.genre {
            labelMovieGenre.text = genre
        } else {
            genreCollection.setValue(true, forKey: "hidden")
        }
        
        if let rating = movieContent.rating {
            self.labelMovieRating.text = rating
        } else {
            self.RatingCollection.setValue(true, forKey: "hidden")
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

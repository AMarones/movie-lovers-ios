//
//  MovieListViewController.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 4/24/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import Spring

class MovieListViewController: UIViewController {

    fileprivate let reuseIdentifier = "MovieCollectionViewCell"
    
    @IBOutlet var collectionView    : UICollectionView!
    @IBOutlet weak var logoImage    : SpringImageView!
    @IBOutlet weak var splashView   : SpringView!
    @IBOutlet weak var errorView    : UIView!
    
    fileprivate var movieList       = [MovieContent]()
    var eventHandler                : MovieListModuleInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventHandler = MovieListPresenter(userInterface: self)
        eventHandler?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        eventHandler?.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        eventHandler?.viewDidAppear()
    }

    fileprivate func sizeForCollectionViewItem() -> CGSize {
        let width = collectionView.bounds.width
        
        var cellWidth = (width - 16) / 3
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            cellWidth = (width - 40) / 6
        }
        
        return CGSize(width: cellWidth, height: 210)
    }
}


extension MovieListViewController: MovieListViewInterface {
    
    func animateLogo() {
        self.logoImage.isHidden = false
        self.logoImage.animate()
    }
    
    func animateSplash() {
        self.splashView.animate()
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    func loadMovieList(_ movieList: [MovieContent]) {
        self.movieList = movieList
        collectionView.reloadData()
    }
    
    func showErrorView() {
        collectionView.isHidden = true
        errorView.isHidden = false
    }
    
    func hideSplash() {
        
    }
    
    func setupNavigationBar() {
        let logo = UIImage(named: "nav_title")
        let imageView = UIImageView(image:logo)
        navigationItem.titleView = imageView
    }
    
    func hideNavgationBar() {
        navigationController?.isNavigationBarHidden = true;
    }
    
    func presentMovieDetailScreenWith(_ movie: MovieContent) {
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, UIScreen.main.scale)
        self.view.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "MoviesListNavigationController") as! MoviesListNavigationController
        controller.backgroundImage = image
        controller.view.layer.cornerRadius = 5.0;
        
        let movieDetailVC = controller.viewControllers.first as! MovieDetailViewController
        movieDetailVC.movieContent = movie
        
        let formSheet = MZFormSheetPresentationViewController(contentViewController: controller)
        formSheet.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.slideFromBottom
        
        let viewSize = self.view.bounds
        let contentViewSize = CGSize(width: viewSize.width - 60, height: viewSize.height - 120)
        formSheet.presentationController?.contentViewSize = contentViewSize//(850,475)
        formSheet.presentationController?.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        formSheet.presentationController?.movementActionWhenKeyboardAppears = MZFormSheetActionWhenKeyboardAppears.moveToTopInset
        formSheet.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheet.presentationController?.shouldCenterVertically = true
        
        formSheet.willPresentContentViewControllerHandler = {
            (contentViewController: UIViewController!) -> Void in
        }
        
        formSheet.didDismissContentViewControllerHandler = {
            (contentViewController: UIViewController!) -> Void in
        }
        
        present(formSheet, animated: true, completion: nil)
    }
}


extension MovieListViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier, for: indexPath) as! MovieCollectionViewCell
        
        let movieContent = movieList[indexPath.row]
        
        cell.setupMovieContent(movieContent)
        
        return cell
    }
}


extension MovieListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return sizeForCollectionViewItem()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        eventHandler?.selectMovieAtIndex(indexPath.row)
    }
}

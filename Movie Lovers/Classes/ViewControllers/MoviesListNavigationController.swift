//
//  MoviesListNavigationController.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 4/24/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import UIKit
import UIView_Positioning

class MoviesListNavigationController: UINavigationController {

    var backgroundImage: UIImage?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let imageView = UIImageView(image: backgroundImage)
        self.view.insertSubview(imageView, at: 0)
        imageView.centerToParent()
        imageView.centerY += 40
        imageView.centerX -= 40
    }
}

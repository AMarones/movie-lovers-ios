//
//  MovieListPresenter.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 30/11/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import Foundation

class MovieListPresenter: NSObject {
    
    var movieListInteractor     : MovieListInteractorInput?
    var userInterface           : MovieListViewInterface?
    
    init(userInterface: MovieListViewInterface) {
        super.init()
        
        self.movieListInteractor = MovieListInteractor(output: self)
        self.userInterface = userInterface
    }
}


extension MovieListPresenter: MovieListModuleInterface {
    
    func viewDidLoad() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.userInterface?.animateLogo()
        }

        movieListInteractor?.findMovieList()
    }
    
    func viewWillAppear() {
        userInterface?.hideNavgationBar()
    }
    
    func viewDidAppear() {
         userInterface?.setupNavigationBar()
    }
    
    func selectMovieAtIndex(_ index: Int) {
        movieListInteractor?.findMovieAtIndex(index)
    }
}


extension MovieListPresenter: MovieListInteractorOutput {
    
    func movieListDidLoad() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            self.userInterface?.animateSplash()
        }
    }
    
    func foundMovieList(_ movieList: [MovieContent]) {
        userInterface?.loadMovieList(movieList)
    }
    
    func movieListNotFound() {
        userInterface?.showErrorView()
    }
    
    func movieListWithInternalError() {
        userInterface?.showErrorView()
    }
    
    func foundMoviePlot(movie: MovieContent) {
        userInterface?.presentMovieDetailScreenWith(movie)
    }
}

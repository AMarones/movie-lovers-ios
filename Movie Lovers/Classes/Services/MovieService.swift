//
//  MovieService.swift
//  Movie Lovers
//
//  Created by Alexandre Marones on 4/24/16.
//  Copyright © 2016 Alexandre Marones. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash

class MovieService: NSObject {

    func loadMovieList(_ success: ((_ movies: [MovieContent]) -> Void)?) -> Void {
        let serverUrl = "https://www.ingresso.com/iphone/ws/IngressoService.svc/rest/Conteudo?SecaoSite=1&Item=7&idCidade=00000002&idPais=1&Pai=S&idItemTemplate=135"
        
        var movies = [MovieContent]()
        
        Alamofire.request(serverUrl).responseString { (data) in
            let xml = SWXMLHash.parse(data.result.value!)
            
            do {
                movies = try xml["ProgramacaoResponse"]["ProgramacaoResult"]["Conteudo"].value()
            } catch {
                // ERROR!
            }
            
            success!(movies)
        }
        
//        Alamofire.request(serverUrl, method: HTTPMethod.get, parameters: nil)
//            .response { (request, response, data, error) in
//                
//                let xml = SWXMLHash.parse(data!)
//                
//                do {
//                    movies = try xml["ProgramacaoResponse"]["ProgramacaoResult"]["Conteudo"].value()
//                } catch {
//                    // ERROR!
//                }
//
//                success!(movies: movies)
//        }
    }
    
}
